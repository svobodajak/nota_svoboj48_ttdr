function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Sends the transports to rescue their target units according to the input TransportTargetMap",
		parameterDefs = {
			{ 
				name = "TransportTargetMap",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "SafeArea",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end


local TRANSPORT_STATE = {
  NONE = nil,
  MOVING_TO_LOAD = 1,
  MOVING_TO_UNLOAD_POSITION = 2,
  WAITING_TO_STOP = 3,
  UNLOADING = 4,
  UNLOADED = 5
}

local WAIT_BEFORE_UNLOAD_TICK_COUNT = 5
local ADDITIONAL_OCCUPATION_MARGIN = -2

function New(self)
  SetupRescueState(self)
end

function Reset(self)
  SetupRescueState(self)
end

function Run(self, unitIds, parameters)
  local transportTargetMap = parameters.TransportTargetMap
  local safeArea = parameters.SafeArea

  for transport, target in pairs(transportTargetMap) do
    if not Spring.ValidUnitID(transport) or not Spring.ValidUnitID(target) then
      transportTargetMap[transport] = nil
      global.rescue.currentlyTaggedForTransport[target] = nil
    elseif self.transportToState[transport] == nil then
      self.transportOccupationRadius[transport] = GetUnitSize(transport) + ADDITIONAL_OCCUPATION_MARGIN
      local x, y, z = Spring.GetUnitPosition(transport)
      self.transportOriginalPosition[transport] = Vec3(x, y, z)
		  Spring.GiveOrderToUnit(transport, CMD.LOAD_UNITS, {target}, {})
      self.transportToState[transport] = TRANSPORT_STATE.MOVING_TO_LOAD

    elseif self.transportToState[transport] == TRANSPORT_STATE.MOVING_TO_LOAD then
      if #Spring.GetCommandQueue(transport) == 0 then
        local occupationRadius = self.transportOccupationRadius[transport]
        local pos = PickUnoccupiedPointInAreaFor(transport, target, safeArea, occupationRadius) 
        MoveToLandingPoint(self, transport, pos)
      end

    elseif self.transportToState[transport] == TRANSPORT_STATE.MOVING_TO_UNLOAD_POSITION then
      local landingArea = {}
       -- checking if we are inside the landing zone instead of checking that 
       -- the command queue is empty, because there is a bug, where the move 
       -- command some times does not finish, even though the transport is
       -- finished moving to the position
      landingArea.center = self.transportLandingPoint[transport]
      landingArea.radius = self.transportOccupationRadius[transport] + 20
      if IsUnitInsideOfArea(transport, landingArea) then
        self.transportToWaitTickCount[transport] = 0
        self.transportToState[transport] = TRANSPORT_STATE.WAITING_TO_STOP
      end

    elseif self.transportToState[transport] == TRANSPORT_STATE.WAITING_TO_STOP then
      self.transportToWaitTickCount[transport] = self.transportToWaitTickCount[transport] + 1
      local occupationRadius = self.transportOccupationRadius[transport]
      if IsPointOccupied(self.transportLandingPoint[transport], occupationRadius, {transport, target}) then
        local pos = PickUnoccupiedPointInAreaFor(transport, target, safeArea, occupationRadius) 
        MoveToLandingPoint(self, transport, pos)
      elseif self.transportToWaitTickCount[transport] >= WAIT_BEFORE_UNLOAD_TICK_COUNT then
        local pos = self.transportLandingPoint[transport]
        Spring.GiveOrderToUnit(transport, CMD.UNLOAD_UNIT, {pos.x, pos.y, pos.z}, {})
        self.transportToState[transport] = TRANSPORT_STATE.UNLOADING
      end

    elseif self.transportToState[transport] == TRANSPORT_STATE.UNLOADING then
      if #Spring.GetCommandQueue(transport) == 0 then
        local pos = self.transportOriginalPosition[transport]
        Spring.GiveOrderToUnit(transport, CMD.MOVE, {pos.x, pos.y, pos.z}, {})
        self.transportToState[transport] = TRANSPORT_STATE.UNLOADED
      end
    end
  end

  local everyoneFinished = true
  for transport, _ in pairs(transportTargetMap) do
    if(self.transportToState[transport] ~= TRANSPORT_STATE.UNLOADED) then
      everyoneFinished = false
    end
  end
  if everyoneFinished then
    return SUCCESS
  end

	return RUNNING;
end

function svodebug(info)
  Spring.Echo(info)
  Spring.Log("PerformRescue", "debug", info)
end

function SetupRescueState(self)
  self.transportToState = {}
  self.transportOriginalPosition = {}
  self.transportToWaitTickCount = {}
  self.transportLandingPoint = {}
  self.transportOccupationRadius = {}
end

function PickRandomPointInArea(safeArea)
  local unitX = (math.random() * 2 - 1) * 0.8
  local unitZ = (math.random() * 2 - 1) * 0.8
  
  local radiusVector = Vec3(unitX * safeArea.radius, 0, unitZ * safeArea.radius)

  return safeArea.center + radiusVector
end

function IsUnitInsideOfArea(unit, area)
	local x, y, z = Spring.GetUnitPosition(unit)
	local unitPosition = Vec3(x, 0, z)
	local center = Vec3(area.center.x, 0, area.center.z)

	local distanceToCenter = unitPosition:Distance(center)

	return distanceToCenter < area.radius
end

function IsPointOccupied(point, radius, exceptUnits)
  local area = {center = point, radius = radius}
  local units = Spring.GetAllUnits()
  local filteredUnits = {}

  for _, except in ipairs(exceptUnits) do
    for __, unit in ipairs(units) do
      if unit ~= except then
        filteredUnits[#filteredUnits + 1] = unit
      end
    end 
  end

  if #exceptUnits == 0 then 
    filteredUnits = units 
  end
  
  for _, unit in ipairs(filteredUnits) do
    if IsUnitInsideOfArea(unit, area) then
      return true
    end
  end

  return false
end

function PickUnoccupiedPointInAreaFor(transport, target, safeArea, radius)
  local point = Vec3(0, 0, 0)
  local tryCount = 0
  local MAX_TRY_COUNT = 100

  repeat
    point = PickRandomPointInArea(safeArea)
    tryCount = tryCount + 1
  until( (not IsPointOccupied(point, radius, {transport, target})) or (tryCount >= MAX_TRY_COUNT))

  return point
end

function GetUnitSize(unit)
  local defId = Spring.GetUnitDefID(unit)
  local unitDef = UnitDefs[defId]
  
  if unitDef.xsize > unitDef.zsize then
    return unitDef.xsize
  else
    return unitDef.zsize
  end
end

function MoveToLandingPoint(self, transport, position)
  Spring.GiveOrderToUnit(transport, CMD.MOVE, {position.x, position.y, position.z}, {})
  self.transportLandingPoint[transport] = position
  self.transportToState[transport] = TRANSPORT_STATE.MOVING_TO_UNLOAD_POSITION
end