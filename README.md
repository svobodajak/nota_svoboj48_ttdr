ttdr (svoboj48)
====
* Fourth HLAA NOTA assignment solution.
* Only the BASE variant
* Implemented behaviour:
    * Iterating over units and filtering which are rescuable
    * Distributing the transports to the rescuable units
    * Rescuing distributed units
    * Sliding units bug workaround (checking the landing zone for other units)
    * Synchronized multiple instances of the Rescue behaviour
* Additional behaviour input:
    * the safe area (this was done due to debugging/convienience reasons)
* Solution does not contain any kind of scouting or enemy avoidance

Behaviours
----
* Rescue

Sensors
----    
* DistributeSourceToTargetUnits
* PickRescuableUnits
* SortByDistanceToPoint

Commands
----
* PerformRescue
* EndMission

Resources
----
* Rescue behaviour and PerformRescue command use icon: 
    * made by [freepik](https://www.freepik.com/) from [www.flaticon.com](https://www.flaticon.com)
    * distributed with the [Flaticon Basic Licence](https://file000.flaticon.com/downloads/license/license.pdf)
* EndMission command uses modified icon:
    * made by [freepik](https://www.freepik.com/) from [www.flaticon.com](https://www.flaticon.com)
    * distributed with the [Flaticon Basic Licence](https://file000.flaticon.com/downloads/license/license.pdf)