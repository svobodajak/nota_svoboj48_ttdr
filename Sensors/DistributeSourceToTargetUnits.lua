local sensorInfo = {
	name = "DistributeSourceToTargetUnits",
	desc = "Distributes the source units to the target units, preferring the closest ones first. This distribution is returned as a map.",
	author = "svobodajak",
	date = "2019-04-23",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(sourceUnits, targetUnits)
  local distributionMap = {}
	
	local maxSize = #sourceUnits < #targetUnits and #sourceUnits or #targetUnits

  for i = 1, maxSize, 1 do
		distributionMap[sourceUnits[i]] = targetUnits[i]
		global.rescue.currentlyTaggedForTransport[targetUnits[i]] = true
  end

  return distributionMap
end