local sensorInfo = {
	name = "PickRescuableUnits",
	desc = "Filters the units on the input based on if they can be rescued or not",
	author = "svobodajak",
	date = "2019-04-23",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function InsideOfArea(unit, area)
	local x, y, z = Spring.GetUnitPosition(unit)
	local unitPosition = Vec3(x, 0, z)
	local center = Vec3(area.center.x, 0, area.center.z)

	local distanceToCenter = unitPosition:Distance(center)

	return distanceToCenter < area.radius
end

return function(unitsToFilter, safeArea)
  local filteredUnits = {}

  for _, unit in ipairs(unitsToFilter) do
    local categoryId = Spring.GetUnitDefID(unit)
		local isTransportable = not UnitDefs[categoryId].cantBeTransported
		local taggedForTransport = (global.rescue.currentlyTaggedForTransport[unit] ~= nil)
		local rescued = InsideOfArea(unit, safeArea)
		if ((not rescued) and isTransportable and (not taggedForTransport)) then
			filteredUnits[#filteredUnits + 1] = unit
		end
  end

  return filteredUnits
end