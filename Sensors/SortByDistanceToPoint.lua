local sensorInfo = {
	name = "SortByDistanceToPoint",
	desc = "Sorts the input units by their distance to the input point. Return a new sorted collection.",
	author = "svobodajak",
	date = "2019-04-23",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


return function(unitsToSort, targetPoint)
  -- already sorted
	if(#unitsToSort <= 1) then
		return unitsToSort
	end

	-- copy unit IDs
	local sortedUnits = {}
	for i = 1, #unitsToSort, 1 do
		sortedUnits[i] = unitsToSort[i]
	end

	-- calculate distance of each unit to the targetPoint
	local targetPosition = Vec3(targetPoint.x, targetPoint.y, targetPoint.z)
	local distances = {}
	for i = 1, #sortedUnits, 1 do
		local x, y, z = Spring.GetUnitPosition(sortedUnits[i])
		local unitPosition = Vec3(x, y, z)
		local distance = unitPosition:Distance(targetPosition)
		distances[#distances + 1] = distance 
	end

	-- insertion sort
	for i = 2, #sortedUnits, 1 do
		local key = distances[i]
		local keyUnitId = sortedUnits[i]
		local j = i - 1
		while((j >= 1) and (distances[j] > key)) do
			distances[j + 1] = distances[j]
			sortedUnits[j + 1] = sortedUnits[j]
			j = j - 1
		end
		distances[j + 1] = key
		sortedUnits[j + 1] = keyUnitId
	end

  return sortedUnits
end